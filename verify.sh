#!/bin/bash
echo "Compiling app.cpp"
g++ app.cpp -o binary

OUTPUT=`./binary`

if [ "$OUTPUT" == "Hello World" ]
then
	echo "OK"
	exit 0
fi
exit 1
