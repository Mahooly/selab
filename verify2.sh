#!/bin/bash
echo "Compiling app2.cpp"
g++ app2.cpp -o binary2

echo "3 4" > /tmp/test1

OUTPUT=`./binary2 < /tmp/test1`

if [ "$OUTPUT" == "7" ]
then
	echo "OK"
	exit 0
fi
exit 1
